
CC = gcc
LIBS = -lSDL2 -lSDL2_image

.PHONY: all run build

all: build

run: rpg
	./$<

build: rpg

rpg: main.cc quadtree.cc
	${CC} -g ${LIBS} -o $@ $^
