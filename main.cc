
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "quadtree.hh"

typedef struct Player
{
  SDL_Texture* texture;
  SDL_Point point;
} Player;

typedef struct State
{
  bool isRunning;
  bool* keysDown;
} State;

SDL_Texture*
loadTexture(SDL_Renderer* renderer, const char* path)
{
  SDL_Texture* texture;

  texture = IMG_LoadTexture(renderer, path);
  assert(texture);
  
  return texture;
}

void
setColor(SDL_Renderer* renderer, SDL_Color color)
{
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}

void
drawTexture(SDL_Renderer* renderer, SDL_Texture* texture, int x, int y)
{
  SDL_Rect dest;
  dest = (SDL_Rect)
    {
      .x = x,
      .y = y,
    };
  SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
  SDL_RenderCopy(renderer, texture, NULL, &dest);
}

State*
createState(void)
{
  State* state;

  state = (State*) malloc(sizeof(State));
  assert(state);
  state->keysDown = (bool*) malloc(sizeof(bool) * SDL_NUM_SCANCODES);
  memset(state->keysDown, 0, sizeof(bool) * SDL_NUM_SCANCODES);
  
  return state;
}

void
destroyState(State* state)
{
  free(state->keysDown);
  free(state);
}

void
drawRect(SDL_Renderer* renderer, SDL_Color color, SDL_Rect rect)
{
  setColor(renderer, color);
  SDL_RenderDrawRect(renderer, &rect);  
}

void
handleEvent(State* state)
{
  SDL_Event event;
  while (SDL_PollEvent(&event))
    {
      switch (event.type)
        {
        case SDL_QUIT:
          state->isRunning = false;
          break;
        case SDL_KEYDOWN:
          state->keysDown[event.key.keysym.scancode] = true;
          break;
        case SDL_KEYUP:
          state->keysDown[event.key.keysym.scancode] = false;
          break;
        }
    }
}

int
main(void)
{
  SDL_Window* window;
  SDL_Renderer* renderer;
  State* state;
  Player* player;

  Quadtree* qt;
  
  assert(SDL_Init(SDL_INIT_EVERYTHING) == 0);
  assert(IMG_Init(IMG_INIT_PNG) != 0);

  window = SDL_CreateWindow("rpg",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            600, 600, SDL_WINDOW_RESIZABLE);
  assert(window != nullptr);

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  assert(renderer);

  player = (Player*) malloc(sizeof(Player));
  assert(player);
  player->texture = loadTexture(renderer, "test.png");
  player->point = { .x = 0, .y = 0 };

  state = createState();
  
  qtMaxSize = 3;
  qt = qtCreate({.x = 100, .y = 100, .w = 400, .h = 400});
  
  state->isRunning = true;

  while (state->isRunning)
    {
      
      setColor(renderer, {.r = 0x00, .g = 0x00, .b = 0x00, .a = 0xff});
      SDL_RenderClear(renderer);

      /* render goes here! */
      drawTexture(renderer, player->texture, player->point.x, player->point.y);
      qtDebugDraw(renderer, qt);
      
      SDL_RenderPresent(renderer);
      handleEvent(state);
    }

  free(player);
  destroyState(state);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
