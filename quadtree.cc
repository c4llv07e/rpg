
#include "quadtree.hh"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

/* QT children indexes:
  #-------------->x
  | [ 0 ] | [ 1 ]
  | -------------
  y [ 2 ] | [ 3 ]
*/

uint32_t qtMaxSize = 1;

bool
rectContainsPoint(SDL_Rect rect, SDL_Point point)
{
  return (point.x >= rect.x && point.x <= (rect.x + rect.w))
    && (point.y >= rect.y && point.y <= (rect.y + rect.h));
}

QtObject*
qtObjectCreate(SDL_Point point, void* obj)
{
  QtObject* object;
  
  object = (QtObject*) malloc(sizeof(QtObject));
  assert(object);
  object->point = point;
  object->object = obj;

  return object;
}

Quadtree*
qtCreate(SDL_Rect rect)
{
  Quadtree* tree;
  
  tree = (Quadtree*) malloc(sizeof(Quadtree));
  assert(tree);
  memset(tree, 0, sizeof(Quadtree));
  tree->rect = rect;

  tree->objects = (QtObject**) malloc(sizeof(QtObject*) * qtMaxSize);
  assert(tree->objects);
  memset(tree->objects, 0, sizeof(QtObject*) * qtMaxSize);
  
  return tree;
}

void
qtDelete(Quadtree* tree)
{
  if (tree->children[0] != nullptr)
    for (int i = 0; i < 4; ++i)
      qtDelete(tree->children[i]);

  free(tree->objects);
  free(tree);
}

bool
qtInsert(Quadtree* tree, QtObject* object)
{
  if (!rectContainsPoint(tree->rect, object->point))
    return false;

  if (tree->objCount < qtMaxSize)
    {
      tree->objects[(tree->objCount)++] = object;
      return true;
    }

  if (tree->children[0] == nullptr)
    qtSplit(tree);

  for (int i = 0; i < 4; ++i)
    if (qtInsert(tree->children[i], object)) return true;
  
  return false;
}

void
qtSplit(Quadtree* tree)
{
  for (int i = 0; i < 4; ++i)
    tree->children[i] = qtCreate({
        .x = tree->rect.x + (tree->rect.w / 2 * (i % 2 != 0)),
        .y = tree->rect.y + (tree->rect.h / 2 * (i > 1)),
        .w = tree->rect.w / 2,
        .h = tree->rect.h / 2,
      });

  for (int i = 0; i < tree->objCount; ++i)
    {
      QtObject* object;
      object = tree->objects[i];
      
      qtInsert(tree->children[(2 * (object->point.y > (tree->rect.y + (tree->rect.h / 2))))
                              + (object->point.x > (tree->rect.x + (tree->rect.w / 2)))],
               object);
    }
  
  free(tree->objects);
}

void
qtDebugDraw(SDL_Renderer* renderer, Quadtree* tree)
{
  SDL_SetRenderDrawColor(renderer, 0xff, 0x00, 0x00, 0xff);
  SDL_RenderDrawRect(renderer, &tree->rect);
  if (tree->children[0] != nullptr)
    for (int i = 0; i < 4; ++i)
      qtDebugDraw(renderer, tree->children[i]);
  else
    {
      SDL_SetRenderDrawColor(renderer, 0x00, 0xff, 0x00, 0xff);
      for (int i = 0 ; i < tree->objCount; ++i)
        SDL_RenderDrawPoint(renderer, tree->objects[i]->point.x, tree->objects[i]->point.y);
    }
}
