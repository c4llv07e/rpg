
#ifndef C4LL_QUADTREE_INC
#define C4LL_QUADTREE_INC

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_rect.h>
#include <stdint.h>

extern uint32_t qtMaxSize;

typedef struct QtObject
{
  SDL_Point point;
  void* object;
} QtObject;

typedef struct Quadtree
{
  SDL_Rect rect;
  uint32_t objCount;
  QtObject** objects;
  Quadtree* children[4];
} Quadtree;

QtObject* qtObjectCreate(SDL_Point point, void* obj);
void qtAddObject(Quadtree* qt, QtObject* object, SDL_Point pos);
void qtSplit(Quadtree* tree);
bool qtInsert(Quadtree* tree, QtObject* object);
void qtDelete(Quadtree* tree);
Quadtree* qtCreate(SDL_Rect rect);
bool rectContainsPoint(SDL_Rect rect, SDL_Point point);
void qtDebugDraw(SDL_Renderer* renderer, Quadtree* tree);

#endif
